package com.exerp.graphqldemo;

import static com.exerp.graphqldemo.model.Currency.USD;
import static com.exerp.graphqldemo.model.Gender.Female;
import static com.exerp.graphqldemo.model.Gender.Male;
import static com.exerp.graphqldemo.model.MembershipType.Cash;
import static com.exerp.graphqldemo.model.MembershipType.EFT;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.exerp.graphqldemo.model.Membership;
import com.exerp.graphqldemo.model.MembershipRepository;
import com.exerp.graphqldemo.model.MembershipType;
import com.exerp.graphqldemo.model.Person;
import com.exerp.graphqldemo.model.PersonRepository;
import com.exerp.graphqldemo.model.Price;

@Component
public class TestData implements CommandLineRunner {
	
	private static final Logger log = LoggerFactory.getLogger(TestData.class);
	
	@Autowired
	private PersonRepository persons;
	
	@Autowired
	private MembershipRepository subscriptions;
	
	@Override
	public void run(String... args) throws Exception {
		Person vera = persons.save(new Person("Vera Hansen", 35, Female));
		persons.save(new Person("Don Johnson", 66, Male));
		addPersonWithMembership(new Person("Dwayne Johnson", 46, Male), "Pump that iron", EFT, 49);
		addPersonWithMembership(new Person("Liz Taylor", 79, Female), "Hardcore workout", EFT, 49);
		
		persons.findAll().forEach(person -> {
			log.warn(person.toString());
		});
		
		persons.findById(1l).get().getMemberships();
		
		subscriptions.save(new Membership(vera, "Hardcore workout", Cash,
				new Price(USD, BigDecimal.valueOf(50))));

		log.warn("All subscriptions");
		subscriptions.findAll().forEach(sub -> {
			log.warn(sub.toString());
		});
		
		log.warn("Vera's subscriptions");
		subscriptions.findAllByPerson(vera).forEach(sub -> {
			log.warn(sub.toString());
		});
	}

	private void addPersonWithMembership(Person person, String membershipName, MembershipType type, float price) {
		persons.save(person);
		subscriptions.save(new Membership(person, membershipName, type, new Price(USD, BigDecimal.valueOf(price))));
	}
}