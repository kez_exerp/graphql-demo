package com.exerp.graphqldemo.resolver;

import java.util.Optional;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.exerp.graphqldemo.model.Person;
import com.exerp.graphqldemo.model.PersonRepository;
import com.exerp.graphqldemo.model.Membership;
import com.exerp.graphqldemo.model.MembershipRepository;
import com.exerp.graphqldemo.model.MembershipType;

public class Query implements GraphQLQueryResolver {
		
    private PersonRepository personRepository;
    
	private MembershipRepository membershipRepository;
	
	public Query(PersonRepository personRepository, MembershipRepository subscriptionRepository) {
		this.personRepository = personRepository;
		this.membershipRepository = subscriptionRepository;
	}

	public Iterable<Person> findAllPersons() {
        return personRepository.findAll();
    }
    
    public Optional<Person> findPersonById(Long id) {
    	return personRepository.findById(id);
    }

//    public Iterable<Person> findPersonsWithMembershipType(MembershipType type) {
//    	return personRepository.findPersonsWithMembershipType(type);
//    }
    public long countPersons() {
        return personRepository.count();
    }
    
    public Iterable<Membership> findAllMemberships() {
    	return membershipRepository.findAll();
    }
    
    public Optional<Membership> findMembershipById(Long id) {
    	return membershipRepository.findById(id);
    }
    
    public long countMemberships() {
    	return membershipRepository.count();
    }
}