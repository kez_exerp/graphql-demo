package com.exerp.graphqldemo.resolver;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

import com.exerp.graphqldemo.model.Currency;
import com.exerp.graphqldemo.model.Price;

public class CurrencyConverter {

	public static CurrencyConverter INSTANCE = new CurrencyConverter();
	
	private Map<Currency, Map<Currency, BigDecimal>> rates = new HashMap<>();
	
	CurrencyConverter() {
		addPair(Currency.USD, Currency.DKK, BigDecimal.valueOf(6.5f));
		addPair(Currency.EUR, Currency.USD, BigDecimal.valueOf(1.3f));
		addPair(Currency.USD, Currency.USD, BigDecimal.valueOf(1f));
		addPair(Currency.EUR, Currency.DKK, BigDecimal.valueOf(7.5f));
		addPair(Currency.DKK, Currency.DKK, BigDecimal.valueOf(1f));
		addPair(Currency.EUR, Currency.EUR, BigDecimal.valueOf(1f));
	}
	
	private void addPair(Currency from, Currency to, BigDecimal rate) {
		add(from, to, rate);
		if (!from.equals(to)) {
			add(to, from, BigDecimal.ONE.divide(rate, 4, RoundingMode.HALF_EVEN));
		}
	}

	private void add(Currency from, Currency to, BigDecimal rate) {
		if (!rates.containsKey(from)) {
			rates.put(from, new HashMap<Currency, BigDecimal>());
		}
		rates.get(from).put(to, rate);
	}

	public Price convert(Price price, Currency currency) {
		return new Price(currency, rates.get(price.getCurrency()).get(currency)
				.multiply(price.getAmount().setScale(4, RoundingMode.HALF_EVEN)));
	}

}
