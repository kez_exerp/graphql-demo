package com.exerp.graphqldemo.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.exerp.graphqldemo.model.Currency;
import com.exerp.graphqldemo.model.Membership;
import com.exerp.graphqldemo.model.Person;
import com.exerp.graphqldemo.model.PersonRepository;
import com.exerp.graphqldemo.model.Price;

public class MembershipResolver implements GraphQLResolver<Membership> {

	private PersonRepository personRepository;

	public MembershipResolver(PersonRepository personRepository) {
		this.personRepository = personRepository;
	}
	
	public Person getPerson(Membership subscription) {
		return personRepository.findById(subscription.getPerson().getId()).get();
	}
	
	public Price getPrice(Membership membership, Currency currency) {
		return CurrencyConverter.INSTANCE.convert(membership.getPrice(), currency);
	}
	
}
