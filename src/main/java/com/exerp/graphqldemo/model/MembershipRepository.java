package com.exerp.graphqldemo.model;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

public interface MembershipRepository extends CrudRepository<Membership, Long> {

	Collection<Membership> findAllByPerson(Person person);
	
}
