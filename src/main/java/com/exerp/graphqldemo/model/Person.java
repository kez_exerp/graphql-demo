package com.exerp.graphqldemo.model;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.springframework.core.style.ToStringCreator;

@Entity
public class Person {
	
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

    private String name;
	
    private int age;
	
    private Gender gender;
    
	@OneToMany(mappedBy = "person")
	private Collection<Membership> memberships;
	
    Person() {}
    
    public Person(String name, int age, Gender gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
	}
    
    public Long getId() {
		return id;
	}
    
    public void addMembership(Membership membership) {
    	memberships.add(membership);
    }
    
    public Collection<Membership> getMemberships() {
    	return memberships;
    }
    
    @Override
    public String toString() {
    	return new ToStringCreator(this)
    			.append("id", id)
    			.append("name", name)
    			.append("age", age)
    			.append("gender", gender)
    			.toString();
    }
}
