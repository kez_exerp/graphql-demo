package com.exerp.graphqldemo.model;

import java.math.BigDecimal;

import org.springframework.core.style.ToStringCreator;

public class Price {
	
	private Currency currency;
	private BigDecimal amount;

	Price() {}
	
	public Price(Currency currency, BigDecimal amount) {
		this.currency = currency;
		this.amount = amount;
	}
	
	public Currency getCurrency() {
		return currency;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	
	@Override
	public String toString() {
		return new ToStringCreator(this)
				.append("currency", currency)
				.append("amount", amount)
				.toString();
	}

}
