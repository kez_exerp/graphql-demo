package com.exerp.graphqldemo.model;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.springframework.core.style.ToStringCreator;

@Entity
public class Membership {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Person person;
    
    private String name;
    
    private MembershipType type;
    
    @Embedded
    private Price price;
    
    
    Membership() {
	}
    
    public Membership(Person person, String name, MembershipType type, Price price) {
    	this.person = person;
		this.name = name;
		this.type = type;
		this.price = price;
	}
    
    public Person getPerson() {
    	return person;
    }
    
    public Price getPrice() {
    	return price;
    }
    
    public void setName(String name) {
    	this.name = name;
    }
    
    @Override
    public String toString() {
    	return new ToStringCreator(this)
    			.append("id", id)
    			.append("name", name)
    			.append("type", type)
    			.append("price", price)
    			.toString();
    }

}
