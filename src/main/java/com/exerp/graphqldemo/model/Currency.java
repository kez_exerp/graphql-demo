package com.exerp.graphqldemo.model;

public enum Currency {
	USD, DKK, EUR
}
