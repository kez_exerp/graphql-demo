package com.exerp.graphqldemo.model;

import java.util.Collection;

import com.coxautodev.graphql.tools.GraphQLResolver;

public class PersonResolver implements GraphQLResolver<Person> {

	private MembershipRepository membershipRepository;

	public PersonResolver(MembershipRepository membershipRepository) {
		this.membershipRepository = membershipRepository;
	}
	
	public Collection<Membership> getMemberships(Person person) {
		return membershipRepository.findAllByPerson(person);
	}
}
