package com.exerp.graphqldemo.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {
	
//	@Query("select ")
//	Iterable<Person> findPersonsWithMembershipType(MembershipType type);

}
