package com.exerp.graphqldemo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.exerp.graphqldemo.model.MembershipRepository;
import com.exerp.graphqldemo.model.PersonRepository;
import com.exerp.graphqldemo.model.PersonResolver;
import com.exerp.graphqldemo.resolver.MembershipResolver;
import com.exerp.graphqldemo.resolver.Mutation;
import com.exerp.graphqldemo.resolver.Query;

import graphql.ExceptionWhileDataFetching;
import graphql.GraphQLError;
import graphql.servlet.GraphQLErrorHandler;

@SpringBootApplication
public class GraphqlDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphqlDemoApplication.class, args);
	}

	@Bean
	public GraphQLErrorHandler errorHandler() {
		return new GraphQLErrorHandler() {
			@Override
			public List<GraphQLError> processErrors(List<GraphQLError> errors) {
				List<GraphQLError> clientErrors = errors.stream()
						.filter(this::isClientError)
						.collect(Collectors.toList());
				List<GraphQLError> serverErrors = errors.stream()
						.filter(e -> !isClientError(e))
						.map(GraphQLErrorAdapter::new)
						.collect(Collectors.toList());
				List<GraphQLError> e = new ArrayList<>();
				e.addAll(clientErrors);
				e.addAll(serverErrors);
				return e;
			}

			protected boolean isClientError(GraphQLError error) {
				return !(error instanceof ExceptionWhileDataFetching || error instanceof Throwable);
			}
		};
	}

	@Bean
	public MembershipResolver subscriptionResolver(PersonRepository authorRepository) {
		return new MembershipResolver(authorRepository);
	}
	
	@Bean
	public PersonResolver personResolver(MembershipRepository membershipRepository) {
		return new PersonResolver(membershipRepository);
	}
	
	@Bean
	public Query query(PersonRepository personRepository, MembershipRepository subscriptionRepository) {
		return new Query(personRepository, subscriptionRepository);
	}

	@Bean
	public Mutation mutation(MembershipRepository membershipRepository) {
		return new Mutation(membershipRepository);
	}
}